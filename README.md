![image](/img/banner.png)

# Pass Word Generator in Fyne

## Downloading for Mac  
In case you have MacOS and did not install go etc...  

There is an excutable file, passwordmine, which compiled for MacOS in compiled folder.  

## Usage 使い方  
This App is not totally unespected string generator. It contains your favorite words or symbols.   
Input what you like and Press EnterKey, the order of elements is randomized.  

好きな文字や記号をスペースで区切って入力したらEnterキーを押す。  

＊ 日本語は入力不可。

![](/img/example.png)
  
Also, I added a desktop feature. If you close the window, the app do not quit until you tap the desktop-icon.  

ウィンドウを閉じてもアプリは終了しない。終了させるにはデスクトップアイコンからQuitを選択する。

## Reference  
https://fyne.io/